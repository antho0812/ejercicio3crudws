#CRUD REST#

Aplicacion que expone metodos java como servicios REST

Rutas de los servicios REST
•	Listar (Get): 
	http://localhost:8090/empresa/listar

•	Insertar (Post):
	http://localhost:8090/empresa/actualizar

•	Actualizar (Put):
	http://localhost:8090/empresa/actualizar

•	Eliminar (Delete)
	http://localhost:8090/empresa/eliminar?id=3

•	Buscar por id (Get)
	http://localhost:8090/empresa/buscar?id=3
