/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.practica.practica.controller;

import com.practica.practica.modelo.Empresa;
import com.practica.practica.negocio.EmpresaDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ANTHONY MARTINEZ
 */
@Controller
@RestController
@RequestMapping("/empresa")
public class EmpresaController {
    
    @Autowired
    public EmpresaDAO empresaDao;
    
    
    @RequestMapping(value = "/listar")
    @ResponseBody
    public List<Empresa> listar(){
        return empresaDao.listar();
    }
    
    @PostMapping(value = "/insertar")
    @ResponseBody
    public ResponseEntity insertar( @RequestBody Empresa empresa){
        try{
            empresaDao.insertar(empresa);
            return ResponseEntity.ok("Persona creada correctamente");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PutMapping(value = "/actualizar")
    public ResponseEntity actualizar( @RequestBody Empresa empresa){
        try{
            empresaDao.actualizar(empresa);
            return  ResponseEntity.ok("Persona actualizada correctamente");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @DeleteMapping(value = "/eliminar")
    public ResponseEntity actualizar(@RequestParam Long id){
        try{
            Empresa empresa = empresaDao.obtenerEmpresaById(id);
            if(empresa == null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Empresa no encontrada");
            }
            empresaDao.eliminar(empresa);
            return  ResponseEntity.ok("Persona eliminada correctamente");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @GetMapping(value = "/buscar")
    @ResponseBody
    public ResponseEntity listar(@RequestParam Long id){
        try{
            Empresa empresa = empresaDao.obtenerEmpresaById(id);
            if(empresa == null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Empresa no encontrada");
            }
             return ResponseEntity.ok(empresa);
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
