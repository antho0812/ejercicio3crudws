/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.practica.practica.negocio;

import com.practica.practica.modelo.Empresa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ANTHONY MARTINEZ
 */
@Repository
public class EmpresaDAO {
    
    @PersistenceContext
    EntityManager em;
    
    @Transactional(readOnly = true)
    public List<Empresa> listar(){
        Query query = em.createQuery("select e from Empresa e");
        return query.getResultList();
    }
    
    @Transactional
    public void insertar(Empresa empresa){
        em.persist(empresa);
    }
    
    @Transactional
    public void actualizar(Empresa empresa){
        em.merge(empresa);
    }
    
    @Transactional
    public void eliminar(Empresa empresa){
        em.remove(empresa);
    }
    
    @Transactional(readOnly = true)
    public Empresa obtenerEmpresaById(Long id){
        Empresa empresa = em.find(Empresa.class, id);
        return empresa;
    }
}
